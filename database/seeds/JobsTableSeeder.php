<?php

use App\Job;
use Illuminate\Database\Seeder;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 1; $i <= 20; $i++) {
            Job::create([
                'name' => $faker->jobTitle,
                'description' => $faker->sentence,
                'skills' => $faker->sentence,
                'published_budget' => $faker->numberBetween($min = 5000000, $max = 15000000),
                'deadline' => '2018-12-01',
                'company_id' => rand(1, 10),
            ]);
        }

    }
}
