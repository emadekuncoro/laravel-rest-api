<?php

use Illuminate\Database\Seeder;
use App\Resume;

class ResumesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 12; $i < 22; $i++) {
            Resume::create([
                'skills' => $faker->randomElement($array = array ('Programming', 'Design Graphic', 'Architect')),
                'study_title' => $faker->randomElement($array = array ('D3', 'S1', 'S2')),
                'language' => $faker->randomElement($array = array ('ID', 'EN')),
                'experience' => rand(1,10),
                'jobseeker_id' => $i,
            ]);
        }
    }
}
