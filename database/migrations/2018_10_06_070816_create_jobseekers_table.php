<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobseekersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobseekers', function (Blueprint $table) {

            $table->string('email')->unique();
            $table->string('first_name');
            $table->string('last_name');
            $table->enum('sex', ['M', 'F']);
            $table->date('date_of_birth')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->string('country')->nullable();
            $table->integer('proposal_count')->unsigned()->default(0);

            $table->integer('rank_id')->unsigned()->default(1);
            $table->foreign('rank_id')->references('id')->on('jobseeker_ranks');
            
            $table->integer('user_id')->unsigned()->primary();
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jobseekers');
    }
}
