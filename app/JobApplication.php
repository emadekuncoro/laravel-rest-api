<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplication extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'applications';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'intro', 
        'budget',
        'apply_date',
        'completion_date',
        'resume_id',
        'job_id',
        'company_id',
    ];

    public function scopeResume($query, $resume)
    {
        return $query->where('resume_id', $resume);
    }
    public function scopeJob($query, $job)
    {
        return $query->where('job_id', $job);
    }
    public function scopeCompany($query, $company)
    {
        return $query->where('company_id', $company);
    }
}
