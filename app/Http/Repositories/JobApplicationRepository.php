<?php

namespace App\Http\Repositories;

use Illuminate\Validation\ValidationException;
use Illuminate\Support\Facades\DB;
use App\JobApplication;

class JobApplicationRepository
{

    protected $jobapplication;

    public function __construct(JobApplication $jobapplication)
    {
        $this->jobapplication = $jobapplication;
    }

    public function get_jobseeker_point(int $user_id)
    {
        $data = DB::table('jobseekers')
            ->join('jobseeker_ranks', 'jobseekers.rank_id', '=', 'jobseeker_ranks.id')
            ->select('jobseekers.proposal_count', 'jobseeker_ranks.points')
            ->where('jobseekers.user_id', $user_id)
            ->first();
            
        return $data;
    }

}
