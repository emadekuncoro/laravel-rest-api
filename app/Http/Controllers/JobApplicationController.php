<?php

namespace App\Http\Controllers;

use App\Http\Resources\JobApplicationResource;
use App\Http\Requests\StoreJobApplication;
use App\Http\Services\JobApplicationService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Company;
use App\Job;
use App\Jobseeker;
use App\JobApplication;

class JobApplicationController extends Controller
{
    protected $JobAppService;

    public function __construct(JobApplicationService $JobAppService)
    {
        $this->JobAppService = $JobAppService;
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return JobApplicationResource::collection(JobApplication::paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreJobApplication $request)
    {
        $before_store = $this->JobAppService->validator($request);
        if ($before_store !== true) {
            return response()->json(['status' => 'error', 'message' => $before_store], 401);
        }

        //save data
        $application = $this->JobAppService->save($request);

        return new JobApplicationResource($application);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $company_id
     * @return \Illuminate\Http\Response
     */
    public function show($company_id)
    {
        $user = auth()->user();
        $user_id = $user->id;
        if ($user->role != 'employer') {
            return response()->json(['status' => 'error', 'message' => 'You have no right to access job application, only employer role allowed..'], 401);
        }

        //check if company_id is belong to user id
        Company::ID($company_id)->User($user_id)->firstOrFail();
        $data = JobApplication::Company($company_id)->paginate(10);
        return JobApplicationResource::collection($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jobApplication = JobApplication::find($id);
        if (!empty($jobApplication)) {
            $jobApplication->delete();
            return response()->json(["status" => 'ok', "message" => "record id:{$id} has been deleted.."], 200);
        } else {
            return response()->json(["status" => 'error', "message" => "record id:{$id} not found"], 200);
        }
    }

}
