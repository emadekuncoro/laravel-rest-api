<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreJobRequest;
use App\Job;
use App\Company;
use App\Http\Resources\JobResource;
use App\Http\Services\JobService;

class JobController extends Controller
{
    protected $jobservice;

    public function __construct(JobService $jobservice)
    {
        $this->jobservice = $jobservice;
        $this->middleware('auth:api')->except(['index', 'show']);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return App\Http\Resources\JobResource;
     */
    public function index()
    {
        return JobResource::collection(Job::paginate(10));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return App\Http\Resources\JobResource;
     */
    public function store(StoreJobRequest $request)
    {
        $validator = $this->jobservice->validator($request);
        if ($validator !== true) {
            return response()->json(['status' => 'error', 'message' => $validator], 401);
        }

        $job = Job::create([
            'name' => $request->name,
            'description' => $request->description,
            'skills' => $request->skills,
            'published_budget' => $request->published_budget,
            'deadline' => $request->deadline,
            'company_id' => $request->company_id,
        ]);

        return new JobResource($job);
    }

     /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $data = Job::findOrFail($id);
        return new JobResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return App\Http\Resources\JobResource;
     */
    public function update(StoreJobRequest $request, int $id)
    {
        $validator = $this->jobservice->validator($request);
        if ($validator !== true) {
            return response()->json(['status' => 'error', 'message' => $validator], 401);
        }

        $job = Job::where('id', $id)
            ->update([
                'name' => $request->name,
                'description' => $request->description,
                'skills' => $request->skills,
                'published_budget' => $request->published_budget,
                'deadline' => $request->deadline,
                'company_id' => $request->company_id,
        ]);

        return new JobResource(Job::findOrFail($id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        $job = Job::find($id);
        if (!empty($job)) {
            $job->delete();
            return response()->json(["status" => 'ok', "message" => "record id:{$id} has been deleted.."], 200);
        } else {
            return response()->json(["status" => 'error', "message" => "record id:{$id} not found"], 200);
        }
    }

}
