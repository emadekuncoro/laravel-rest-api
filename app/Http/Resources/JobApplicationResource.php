<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class JobApplicationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'intro' => $this->intro,
            'budget' => $this->budget,
            'apply_date' => (string) $this->apply_date,
            'completion_date' => (string) $this->completion_date,
            'resume_id' => $this->resume_id,
            'job_id' => (string) $this->job_id,
            'company_id' => (string) $this->company_id,
            'status' => (string) $this->status,
            'created_at' => (string) $this->created_at,
            'updated_at' => (string) $this->updated_at,
          ];
    }
}
