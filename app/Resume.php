<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resume extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'resumes';

     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'skills', 'study_title', 'language', 'experience',
    ];
}
